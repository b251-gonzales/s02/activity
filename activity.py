year = int(input("Please input a year\n"))

if year <= 0:
	print(f"Invalid input, year cannot be 0 and below!")
elif year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")


num_row = int(input(f"Enter number of rows\n"))
num_col = int(input(f"Enter number of columns\n"))


for x in range(num_row):
	for y in range(num_col):
		if y == num_col - 1:
			print(f"*")
		else:
			print(f"*", end='')
